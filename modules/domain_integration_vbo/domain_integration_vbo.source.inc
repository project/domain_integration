<?php
/**
 * @file
 * Domain Integration Views Bulk Operations Node source Actions.
 */

module_load_include('inc', 'domain_integration_rules', 'domain_integration_rules.rules');

/**
 * domain_integration_vbo_modify_node_source_domains: Configuration form.
 */
function domain_integration_vbo_modify_node_source_domains_form($settings, &$form_state) {
  $form = array();

  $options_list = array();
  // Add active domains.
  foreach (domain_domains() as $domain) {
    $options_list[$domain['domain_id']] = $domain['sitename'];
  }
  $form['domain_source_settings'] = array(
    '#type' => 'radios',
    '#title' => t('Select source domain'),
    '#options' => $options_list,
    '#description' => t("New source domain settings that will be applied on all selected nodes."),
    '#required' => TRUE,
  );

  return $form;
}

/**
 * domain_integration_vbo_modify_node_source_domains: Configuration form submit.
 */
function domain_integration_vbo_modify_node_source_domains_submit($form, $form_state) {
  $return['domain_source_settings'] = $form_state['values']['domain_source_settings'];

  return $return;
}

/**
 * domain_integration_vbo_modify_node_source_domains: Action callback.
 */
function domain_integration_vbo_modify_node_source_domains(&$node, $context) {

  // Direct call to Rules Action callback-function.
  domain_integration_rules_rules_action_domain_source_node($node, $context['domain_source_settings']);
}
